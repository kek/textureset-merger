# Textureset Merger

This project provides a simple python script to merge Substance Painter texture sets into a single one

## Getting started

For now, the script only works if you export using transparent background. It will search in BaseColor non empty pixels to create a mask and apply this mask to all textures.

First thing to do is to download the 2 dependencies: Pillow and numpy:

```
pip3 install pillow
pip3 install numpy
```

Then, simply run 
```
python3 texturesetmerger.py path/to/your/output/folder
```
and voila...

# Known issues

- Only 8-bit per channel are supported for now... which is quite sad for normal maps.
- All maps must have the same resolution


# Contributing

If you are willing to contribute, please do not hesitate to clone this repository and create pull requests.

If you are not fluent in python but have good feature ideas, don't hesitate to open issues.

# License

Do whatever you like. There is no license... now if running this script crashes your computer or erase your nice maps, I am sorry, but can't be blamed.

