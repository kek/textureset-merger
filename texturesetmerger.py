from PIL import Image

import argparse
import glob
from os import path
import numpy as np

def longest_string_start(strings):
    result = ''
    length = len(min(strings, key=len)) - 1
    while length > 0:
        match = strings[0][0:length]
        found = True
        for string in strings:
            found = string[0:length] == match
            if not found:
                length = length - 1
                break
        if found:
            return match
    return ''

def get_base_file_name(file_path, pattern):
    file_name = path.basename(file_path)
    return file_name[0:file_name.index(pattern)]

def build_masks(texture_set_path, mask_name):
    print("build masks")
    pattern = path.join(texture_set_path,f'*{mask_name}*')
    files = glob.glob(pattern)
    masks = {}
    for file in files:
        base_filename = get_base_file_name(file, mask_name)
        with Image.open(file) as img:
                pixels = np.array(img)
                non_empty_pixels_mask = np.any(pixels != [0, 0, 0, 0], axis=-1)
                masks[base_filename] = non_empty_pixels_mask
    return masks

def process_channel(texture_set_path, channel, masks):
    print(f"process {channel}")
    mode_to_bpp = {'1':0, 'L':1, 'P':1, 'RGB':3, 'RGBA':4, 'CMYK':4, 'YCbCr':3, 'I':4, 'F':4}
    pattern = path.join(texture_set_path,f'*{channel}*')
    files = glob.glob(pattern)
    file_names = [path.basename(f) for f in files]
    with Image.open(files[0]) as img:
        size = img.size
        mode = img.mode
        bpp = mode_to_bpp[img.mode]
        pixels = np.array(img)
    if bpp == 1:
        output_array = np.zeros((size[0], size[1]))
    else:
        output_array = np.zeros((size[0], size[1],bpp))
    for file in files:
        base_filename = get_base_file_name(file, channel)
        with Image.open(file) as img:
            pixels = np.array(img)
            non_empty_pixels_mask = np.copy(masks[base_filename])
            output_array[non_empty_pixels_mask] = pixels[non_empty_pixels_mask]
    output_image = Image.fromarray(np.uint8(output_array))
    output_file_name = longest_string_start(file_names)
    output_image.save(path.join(texture_set_path,f'{output_file_name}{channel}.png'))

def main():
    parser = argparse.ArgumentParser(description='Merge texture sets.')
    parser.add_argument('path', metavar='Path', type=str, nargs='+',
                    help='Path to texture sets folder')
    args = parser.parse_args()
    texture_set_path = args.path[0]
    channels = ['BaseColor', 'Normal', 'Roughness', 'Metallic' , 'Height']
    mask = 'BaseColor'
    
    masks = build_masks(texture_set_path, mask)
    
    for channel in channels:
        process_channel(texture_set_path, channel, masks)
    return 0

main()